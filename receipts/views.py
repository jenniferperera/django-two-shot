from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_home(request):
    home_receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_home": home_receipt,
    }
    return render(request, "receipts/receipt_home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    home_expense = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_home": home_expense,
    }
    return render(request, "receipts/category_home.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "create_category": form
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def account_list(request):
    account_home = Account.objects.filter(owner=request.user)
    context = {
        "account_home": account_home,
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "create_account": form
    }
    return render(request, "receipts/create_account.html", context)
